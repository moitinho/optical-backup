module gitlab.com/moitinho/optical-backup

go 1.15

require (
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.1.1
)
