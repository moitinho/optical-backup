build:
	go build

release:
	go build -ldflags="-s -w"
	upx --best optical-backup

test:
	go test -coverprofile=coverage.out
	go tool cover -html coverage.out -o coverage.html
