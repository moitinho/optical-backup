package main

import (
	"gitlab.com/moitinho/optical-backup/cmd"
)

func main() {
	cmd.Execute()
}
