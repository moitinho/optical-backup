package internal

import (
	"os"
)

// Snapshot represents a state of the backup target in time.
type Snapshot struct {
	name  string
	Meta  map[string]string `json:"meta"`
	Files map[string]string `json:"files"`

	fileList []File
}

// NewSnapshot is the constructor for a Snapshot
func NewSnapshot(name string) *Snapshot {
	// TODO
	return &Snapshot{
		name:  name,
		Meta:  make(map[string]string),
		Files: make(map[string]string),
	}
}

func (s *Snapshot) visit(path string, info os.FileInfo, err error) error {
	if !shouldVisit(info) {
		return nil
	}

	s.fileList = append(s.fileList, File{
		path: path,
		size: info.Size(),
	})
	return nil
}

func shouldVisit(info os.FileInfo) bool {
	// Do not look into symlinks
	// Do not look into directories
	if info.Mode()&(os.ModeSymlink|os.ModeDir) != 0 {
		return false
	}

	return true
}
