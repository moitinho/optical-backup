package internal

import (
	"encoding/json"
	"os"
)

// Index represents a set of snapshots and discs
type Index struct {
	// TODO
	Config    Config               `json:"config"`
	Discs     map[string]*Disc     `json:"discs"`
	Snapshots map[string]*Snapshot `json:"snapshots"`
}

// NewIndex constructor
func NewIndex() *Index {
	// TODO
	return &Index{
		Config:    *NewConfig("sha256"),
		Discs:     make(map[string]*Disc),
		Snapshots: make(map[string]*Snapshot),
	}
}

// LoadIndex from somewhere
func LoadIndex(path string) *Index {
	// TODO
	return &Index{}
}

func (i *Index) save() error {
	// TODO
	jsonData, err := json.Marshal(i)
	if err != nil {
		return err
	}

	file, err := os.Create("test.json")
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write(jsonData)
	if err != nil {
		return err
	}
	return file.Sync()
}
