package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"time"
)

// Full performs a full backup
func Full(args []string) {
	// loads an index file
	index := NewIndex()

	path := "internal"
	t := time.Now()
	name := t.Format("2006-12-31 23:59")
	index.Snapshots[name] = NewSnapshot(name)
	s := index.Snapshots[name]

	s.fileList = []File{}

	// Scan the origin folder to create the file list with sizes
	err := filepath.Walk(path, s.visit)
	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", path, err)
		return
	}

	for f := range s.fileList {
		s.Files[s.fileList[f].path], err = s.fileList[f].hash()
	}

	// Sort the files by size so that the large ones can be stored first in
	// order to achieve better packing
	sort.Sort(BySizeRev(s.fileList))

	discName := "0001"
	disc := NewDisc(discName)
	index.Discs[discName] = disc

	os.MkdirAll(disc.name+"/data", 0700)
	os.MkdirAll(disc.name+"/tmp", 0700)
	for f := range s.fileList {
		// Stage file
		disc.stage(&s.fileList[f])
	}

	index.save()
}
