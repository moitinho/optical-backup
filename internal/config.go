package internal

// Config holds the configuration structure
type Config struct {
	// TODO
	Hash string `json:"hash"`
}

// NewConfig generator for Config
func NewConfig(hash string) *Config {
	// TODO
	if hash == "" || hash == "sha256" {
		hash = "sha256"
	}
	return &Config{
		Hash: hash,
	}
}

// LoadConfig Config loader
func LoadConfig() *Config {
	// TODO
	return &Config{}
}
