package internal

import (
	"io"
	"os"
)

// File is a backed up file along with all its data
type File struct {
	path  string
	_hash string
	size  int64
}

func (f *File) hash() (string, error) {
	if f._hash == "" {
		hash, err := computeHash(f.path)
		f._hash = hash

		if err != nil {
			return "", err
		}
	}
	return f._hash, nil
}

// BySizeRev Helper to sort files by size
type BySizeRev []File

func (a BySizeRev) Len() int           { return len(a) }
func (a BySizeRev) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a BySizeRev) Less(i, j int) bool { return a[i].size > a[j].size }

// Copy a file
func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}
