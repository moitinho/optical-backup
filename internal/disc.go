package internal

import (
	"github.com/satori/go.uuid"
	"math"
	"os"
)

// Disc represents a physical medium
type Disc struct {
	name    string
	Meta    map[string]string `json:"meta"`
	Objects []string          `json:"objects"`
	Size    DiscSize          `json:"size"`
}

// DiscSize represents a type of physical medium
type DiscSize struct {
	Max   int64 `json:"max"`
	Block int64 `json:"block"`
	Usage int64 `json:"usage"`
}

// NewDiscSize constructor for the disc size
func NewDiscSize(format string) DiscSize {
	media := map[string]int64{
		"cd":   int64(700 * math.Pow(1024, 2)),
		"dvd":  int64(4.7 * math.Pow(1024, 3)),
		"bd-r": int64(25 * math.Pow(1024, 3)),
	}

	return DiscSize{
		Max:   media[format],
		Block: 2 * 1024,
		Usage: 0,
	}
}

// NewDisc constructor for a disc
func NewDisc(name string) *Disc {
	// TODO

	return &Disc{
		name:    name,
		Meta:    make(map[string]string),
		Size:    NewDiscSize("bd-r"),
		Objects: []string{},
	}
}

func (disc *Disc) stage(f *File) {
	u := uuid.NewV4()
	tmpPath := disc.name + "/tmp/" + u.String()

	// Copy file to staging environment
	Copy(f.path, tmpPath)

	// Hash it
	hash, _ := f.hash()

	// Re-check the file size since it may have changed since scan
	fi, _ := os.Stat(tmpPath)
	f.size = fi.Size()

	// Rename it to match the hash
	os.Rename(tmpPath, disc.name+"/data/"+hash)

	// Add file to the objects list of the disc
	disc.Objects = append(disc.Objects, hash)
	if f.size > disc.Size.Block {
		disc.Size.Usage += f.size
	} else {
		disc.Size.Usage += disc.Size.Block
	}
}
