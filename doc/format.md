# Data format

## File structure

Every optical disc needs to have the index of all the versions of every single file available.

```
├── data
|   ├── c6
|   |   └── c6f5841a8d6f6e1c6bdd3ce8074a128384defbd68ce6330c9aa1491534af4371
|   └── 74
|       └── 7455906a74ce7e94f818556c2b8a86c9af21b11641ef84457912ae4b643d545f
└── indexes
    ├── 2020-12-29T1544
    └── 2021-01-02T1511
```

## Index format

There are 4 main "things" in this structure:

- An index - the definition of structure, without it, it's all just random files without extensions.
- Disc - a block of data that has files in it, both data and indexes.
- Snapshot - a backup that defines structure for files that are in one or more discs.
- Object - a file that lives in a disc (identified by the hash)

```json
{
  "config": {
    "hash": "sha256"
  },
  "discs": {
    "0001": {
      "meta": {},
      "size": {
        "max": 20000000000,
        "block": 2000,
        "used": 10000
      },
      "objects": [
        "c6f5841a8d6f6e1c6bdd3ce8074a128384defbd68ce6330c9aa1491534af4371"
      ]
    },
    "0002": {
      "meta": {},
      "objects": [
        "7455906a74ce7e94f818556c2b8a86c9af21b11641ef84457912ae4b643d545f"
      ]
    }
  },
  "snapshots": {
    "2020-12-29T15:44": {
      "complete": true,
      "path": "/home/miguel",
      "exclude": [],
      "meta": {
        "tags": [],
        "hostname": "super-awesome-desktop"
      },
      "files": {
        "/home/miguel/Pictures/2020/12/DCIM0000.jpg": "c6f5841a8d6f6e1c6bdd3ce8074a128384defbd68ce6330c9aa1491534af4371"
      }
    },
    "2021-01-02T15:11": {
      "complete": false,
      "path": "/home/miguel/Pictures",
      "exclude": [".bak"],
      "meta": {
        "tags": [],
        "hostname": "pretty-slow-laptop"
      },
      "files": {
        "/home/miguel/Pictures/2020/12/DCIM0000-orig.jpg": "c6f5841a8d6f6e1c6bdd3ce8074a128384defbd68ce6330c9aa1491534af4371",
        "/home/miguel/Pictures/2020/12/DCIM0000.jpg": "7455906a74ce7e94f818556c2b8a86c9af21b11641ef84457912ae4b643d545f"
      }
    }
  }
}
```

### Fields

Config:

- hash: hash used for the files checksum.

Disc:

- meta: assorted metadata
- size:
  - max: max size of the disc
  - block: block size
  - used: used size of data
- objects: array of hashes of files that are stored on the disc.

Snapshot:

- complete: can be either true or false if the snapshot still has more discs after the one where this specific index file has been writen to.
- path: the base path that has been backed up.
- exclude: an array of the exclude patterns that were used when performing the backup
- meta: assorted metadata
  - tags: an array of strings of whatever can be useful.
  - hostname: the hostname of the machine that created the backup.
