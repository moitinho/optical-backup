# Optical Backups
Should be able to do incremental backups. Full backups is basically just starting from scratch.

Should detect file moving, and be able to dedup based on the file hash/size instead of just the path and hash.

Discs should be numbered or named sequentialy, so it should start for eg in `0000` and move on to `0001` and so on.
This will make it extremely easy to figure out missing discs

## Options
1. Snapshot
	1. Full
	2. Incremental
2. List snapshots

### Snapshots
#### Full
```
optical-backup full
```

#### Incremental
```
optical-backup incremental /media/user/cdrom/
```

## Disc redundancy
We can not fill up the disc to the brim so that it can be augmented with ECC data with something like dvdisaster's RS02 or RS03.
The disc augmentation feature of dvdisaster is not compatible with some dvd burning software that ignore the invisible, non filesystem, data that is added to the image.
They recommend writing with K3b, but at least where I read the documentation, support for BD-R is not explicitly supported, so your milage may vary until tested.
This is most likely not compatible with multi-session discs though.

## Issues
### File size limit
Files won't be split into multiple discs, this puts a hard limit on files that are larger than the medium being backed up to.

### File packing
To avoid splitting files in half, if a snapshot needs more than one disc and is composed of big files, a large parte of a disc may be left empty.
This can be avoided by choosing files that can fit nicely into the disc.
The simplest way we can achieve this is by sorting the files by decreasing size and start backing them up in order, and skip files that wouldn't until we reach the end of the list, and then use the remaining list for the next disc and so on...

### Hash colision
There is an extremely slim chance of clashes of different files when using the hash of it.
The default hashing function used will be `sha256`, but will be configurable to please the truly paranoid to allow for functions such as `sha512`.

## Feature wishlist
### Restore
Restoring a directory should require changing to a specific disk only once, and in a reverse chronologic way ideally, so, if I restore a directory based on the index of the latest disc, then it should build a list of which files it needs from each disc, and request for only the needed discs to be inserted, one at a time (or automate it if you ever get a bot to do it).

### Refresh disc
Ability to move files from old discs to new ones, effectively getting rid of discarded data.

### Ignore patterns
Like a gitignore, or `restic`'s exclude patterns.
Ignore patterns should be stored along the snapshot metadata so that the reason for a file non being backed up can be audited.

### Snapshot host information
It may be interesting to, down the road, augment the snapshot info with the host name where the snapshot was taken.
This may help with multi host backups.
