package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/moitinho/optical-backup/internal"
)

func init() {
	rootCmd.AddCommand(incrementalCmd)
}

var incrementalCmd = &cobra.Command{
	Use:   "incremental",
	Short: "Perform an incremental backup",
	Run:   incremental,
}

func incremental(cmd *cobra.Command, args []string) {
	internal.Incremental(args)
}
