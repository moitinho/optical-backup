package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/moitinho/optical-backup/internal"
)

func init() {
	rootCmd.AddCommand(fullCmd)
}

var fullCmd = &cobra.Command{
	Use:   "full",
	Short: "Perform a full backup",
	Run:   full,
}

func full(cmd *cobra.Command, args []string) {
	internal.Full(args)
}
