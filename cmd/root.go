package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var (
	// Used for flags.
	// cfgFile     string
	// userLicense string

	rootCmd = &cobra.Command{
		Use:   "optical-backup",
		Short: "A backup tool for WORM media",
		Long: `Backup tool form WORM media.
	This is mainly intended for having long lived backups on write
	once, read many media, such as optical BluRay discs, while trying
	to go easy on future "archeological" expeditions.
	The objective is not to be the most secure thing ever, but instead
	to provide a sane backup method to store your family photos that
	can be dug out by your grand-grand children.`,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	// cobra.OnInitialize()

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cobra.yaml)")
	// rootCmd.PersistentFlags().StringP("author", "a", "YOUR NAME", "author name for copyright attribution")
	// rootCmd.PersistentFlags().StringVarP(&userLicense, "license", "l", "", "name of license for the project")
}

func er(msg interface{}) {
	fmt.Println("Error:", msg)
	os.Exit(1)
}
